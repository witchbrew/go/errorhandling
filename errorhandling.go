package errorhandling

import "fmt"

type ErrorType byte

const (
	BadInput ErrorType = iota
	NotFound
	Unknown
)

type HandledError struct {
	Type    ErrorType
	Message string
}

func (e *HandledError) Error() string {
	return e.Message
}

func Error(errorType ErrorType, message string) *HandledError {
	return &HandledError{
		Type:    errorType,
		Message: message,
	}
}

func Errorf(errorType ErrorType, format string, args ...interface{}) *HandledError {
	return Error(errorType, fmt.Sprintf(format, args...))
}

func BadInputError(message string) *HandledError {
	return Error(BadInput, message)
}

func BadInputErrorf(format string, args ...interface{}) *HandledError {
	return Errorf(BadInput, format, args...)
}

func NotFoundError(message string) *HandledError {
	return Error(NotFound, message)
}

func NotFoundErrorf(format string, args ...interface{}) *HandledError {
	return Errorf(NotFound, format, args...)
}
